# Cloud002 Info

- Cloud name : boxweb.local
- Users: ubuntu | 12345678 , vagrant | ssh
- OS : Ubuntu 16.0.4 LTS

```
vagrant up --provider=virtualbox
vagrant suspend
vagrant up
vagrant ssh
```

## Change user password

```bash
sudo su
passwd ubuntu
123456
exit
vagrant reload
```

## Change login method from ssh key to password auth

```json
:enable_password_login: true
:ssh_username: "ubuntu"
:ssh_password: "123456"
```

```bash
nano /etc/ssh/sshd_config
# PasswordAuthentication yes
# PermitRootLogin yes
sudo service ssh restart
sudo systemctl status sshd
```


# Install common stacks

- [] DB:Redis

## DB:Redis

- [x] Introduction
- [x] Set up a simple Redis instance
- [x] Common operations:  starting, connecting, shutting down a Redis server

### Introduction

```quote
- Redis is a very popular, memory-based, lightweight key-value database. Strictly
speaking, Redis is a data structure server ( REmote DIctionary Server )

- Redis is an open software written in pure C language so that we can install it by
compilation. Major operating systems also include Redis binary packages in
their software repository, although the Redis version is often a little out of date

-In order to get the latest version of Redis, we will be compiling
and installing the software from source. Before we download the code,
we need to satisfy the build dependencies so that we can compile the software

- To do this, we can install the build-essential meta-package from the Ubuntu repositories.
We will also be downloading the tcl package, which we can use to test our binaries.
```

You can take a look at these ranking pages:

- https://db-engines.com/en/ranking
- https://db-engines.com/en/ranking/key-value+store

- [Official](https://redis.io/)
- [Tutorial](http://try.redis.io/)
- [Quick start](https://redis.io/topics/quickstart)
- [HashTable](https://kipalog.com/posts/Hashtable-in-a-nutshell---Phan-1)
- [Dictionary](https://www.howkteam.vn/course/dictionary-trong-c/dictionary-trong-c-1572)
- [Install](https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-redis-on-ubuntu-16-04)
- [Bin Explaination](https://www.linuxnix.com/linux-directory-structure-explained-bin-folder/)
- [Linux Binary](https://www.interserver.net/tips/kb/linux-binary-directories-explained/)
- [http://www.informit.com/articles/article.aspx?p=2209015&seqNum=2](http://www.informit.com/articles/article.aspx?p=2209015&seqNum=2)
- [file structure](https://www.thegeekstuff.com/2010/09/linux-file-system-structure)
- [systemd replaces init in linux](https://www.tecmint.com/systemd-replaces-init-in-linux/)

More about service and system:

- https://medium.com/@benmorel/creating-a-linux-service-with-systemd-611b5c8b91d6
- https://www.techrepublic.com/article/how-to-start-stop-and-restart-services-in-linux/

UI Tools:

- https://redisdesktop.com/

### Set up a simple Redis instance


```bash
sudo apt-get update
sudo apt-get install build-essential tcl -y
cd $HOME
mkdir redis
pwd
wget http://download.redis.io/releases/redis-5.0.2.tar.gz --verbose -P /tmp
cd /tmp
tar -zxvf redis-5.0.4.tar.gz
cd redis-5.0.4
sudo mkdir -p /etc/redis/
sudo cp redis.conf /etc/redis/
sudo make install
ls /usr/local/bin # default
printf "%s\n", $PATH # check PATH
# redis-benchmark  redis-check-aof  redis-check-rdb  redis-cli  redis-sentinel  redis-server
# copy
cp /tmp/redis-5.0.4/redis.conf /etc/redis/redis.conf
# check
cat $HOME/redis/redis.conf | grep port # Default: port 6379
cat $HOME/redis/redis.conf | grep bind # Default: bind 127.0.0.1
which redis-server
which redis-cli
# run
redis-server redis/redis.conf
# stop
```

### Setup service


This can be used to daemonize anything that would normally run in the foreground; I picked Redis. Put this in `/etc/systemd/system/redis.service`:

```
[Unit]
Description=Redis
After=syslog.target

[Service]
ExecStart=/usr/local/bin/redis-server /etc/redis/redis.conf
RestartSec=5s
Restart=on-success

[Install]
WantedBy=multi-user.target
```

Make sure that `redis.conf` has `demonize no` (the default; systemd will take care of 'daemonizing'). The `Restart=on-success` in the service file means that the daemon will be auto-restarted only when it exited cleanly (so that 'bad' problems are not masked; see [doc](http://www.freedesktop.org/software/systemd/man/systemd.service.html#Restart=)). Then run:

```
sudo systemctl enable /etc/systemd/system/redis.service
sudo systemctl start redis.service
```

Links:
- [intro (coreos)](https://coreos.com/docs/launching-containers/launching/getting-started-with-systemd/)
- [more (digitalocean)](https://www.digitalocean.com/community/tutorials/understanding-systemd-units-and-unit-files)
- [manpage](http://www.freedesktop.org/software/systemd/man/systemd.service.html)


### Sample

```bash
redis-cli -h boxweb.local -n 1 # connect to host boxweb.local, database index = 1( db index start from 0)
redis-cli
INFO # ALL INFO
INFO CLIENTS # INFO OF CLIENTS SECTION
INFO Memory
```

### Secure your redis with password

- [Ref](https://www.digitalocean.com/community/tutorials/how-to-secure-your-redis-installation-on-ubuntu-18-04)
- [Official](https://redis.io/topics/security)


### Protocol

```quote
Redis clients communicate with the Redis server using a protocol called RESP (REdis Serialization Protocol). While the protocol was designed specifically for Redis, it can be used for other client-server software projects.

A client connects to a Redis server creating a TCP connection to the port 6379.

While RESP is technically non-TCP specific, in the context of Redis the protocol is only used with TCP connections (or equivalent stream oriented connections like Unix sockets).
```

- [Offical](https://redis.io/topics/protocol)

### Datatype

- [Official](https://redis.io/topics/data-types)

- Using the string data type
- Using the list data type
- Using the hash data type
- Using the set data type
- Using the sorted set data type
- Using the HyperLogLog data type
- Using the Geo data type
- Managing keys

**1.String**

```bash
redis-cli
127.0.0.1:6379> SET "Domino Pizza" "so 40 D2, Quan Binh Thanh"
OK
127.0.0.1:6379> GET "DOMINO PIZZA"
(nil)
127.0.0.1:6379> GET "Domino Pizza"
"so 40 D2, Quan Binh Thanh"
127.0.0.1:6379> STRLEN "Do"
(integer) 0
127.0.0.1:6379> STRLEN "Domino Pizza"
(integer) 25
127.0.0.1:6379> APPEND "Domino Pizza" ", TP.Ho Chi Minh"
(integer) 41
127.0.0.1:6379> GET "Domino Pizza"
"so 40 D2, Quan Binh Thanh, TP.Ho Chi Minh"
127.0.0.1:6379> SETRANGE "Domino Pizza" 3 "41"
(integer) 41
127.0.0.1:6379> GET "Domino Pizza"
"so 41 D2, Quan Binh Thanh, TP.Ho Chi Minh"
127.0.0.1:6379> SETNX "Pizza Hut" "92 Nguyen Huu Canh, Phuong 22, Quan Binh Thanh, TP.HCM"
(integer) 1
127.0.0.1:6379> SETNX "Domino Pizza" "92 Nguyen Huu Canh, Phuong 22, Quan Binh Thanh, TP.HCM"
(integer) 0
127.0.0.1:6379> MGET "Domino Pizza" "Pizza Hut"
1) "so 41 D2, Quan Binh Thanh, TP.Ho Chi Minh"
2) "92 Nguyen Huu Canh, Phuong 22, Quan Binh Thanh, TP.HCM"
127.0.0.1:6379> MSET "KFC" "Popoyes" "30 d2 Binh Thanh" "31 D2 Binh Thanh" # wrong way for mset
OK
127.0.0.1:6379> MGET "Popoyes" "KFC"
1) (nil)
2) "Popoyes"
127.0.0.1:6379> MSET "KFC" "30 d2 Binh Thanh" "Popoyes" "31 D2 Binh Thanh" # right way: MSET key1 value1 key2 value
OK
127.0.0.1:6379> MGET "Popoyes" "KFC"
1) "31 D2 Binh Thanh"
2) "30 d2 Binh Thanh"
127.0.0.1:6379>
```

```quote
It is worth mentioning how strings are encoded in Redis objects internally. Redis
uses three different encodings to store string objects and will decide the
encoding automatically per string value:
int: For strings representing 64-bit signed integers
embstr: For strings whose length is less or equal to 44 bytes (this used to be
39 bytes in Redis 3.x); this type of encoding is more efficient in memory
usage and performance
raw: For strings whose length is greater than 44 bytes
```

```bash
redis-cli
127.0.0.1:6379> SET foo 123456
OK
127.0.0.1:6379> OBJECT ENCODING foo
"int"
127.0.0.1:6379> SET fee "123 mixed"
OK
127.0.0.1:6379> OJBECT ENCODING fee
(error) ERR unknown command `OJBECT`, with args beginning with: `ENCODING`, `fee`,
127.0.0.1:6379> OBJECT ENCODING fee
"embstr"
127.0.0.1:6379> SET feelong "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQR" # 44 chars
OK
127.0.0.1:6379> OBJECT ENCODING feelong
"embstr"
127.0.0.1:6379> SET feelong "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRS" # 45 chars
OK
127.0.0.1:6379> OBJECT ENCODING feelong
"raw"
```

**2.List**

```c
typedef struct Student{
    char fullname[65];
    char placeOfBirth[65];
    char identity[50];
    struct Student *next;
}
```

```bash
ubuntu@boxweb:~$ redis-cli -h localhost -n 0 -p 6379
localhost:6379> LPUSH students "Nguyen Van An" "Nguyen Dinh Bao" "Le Minh Cao" "Tran Trung Duc" "Cao Van Do" "Pham Thi Chung" "Tran Minh Quyen" "Dao Minh Di" "Tan Minh Sang" "Vo Van Vu"
(integer) 10
localhost:6379> LRANGE students 0 -1
 1) "Vo Van Vu"
 2) "Tan Minh Sang"
 3) "Dao Minh Di"
 4) "Tran Minh Quyen"
 5) "Pham Thi Chung"
 6) "Cao Van Do"
 7) "Tran Trung Duc"
 8) "Le Minh Cao"
 9) "Nguyen Dinh Bao"
10) "Nguyen Van An"
localhost:6379> RPUSH students "Tran Minh Sang"
(integer) 11
localhost:6379> LRANGE students 0 -1
 1) "Vo Van Vu"
 2) "Tan Minh Sang"
 3) "Dao Minh Di"
 4) "Tran Minh Quyen"
 5) "Pham Thi Chung"
 6) "Cao Van Do"
 7) "Tran Trung Duc"
 8) "Le Minh Cao"
 9) "Nguyen Dinh Bao"
10) "Nguyen Van An"
11) "Tran Minh Sang"
localhost:6379> LPUSH students "Nguyen Minh Trung"
localhost:6379> LRANGE students 0 -1
 1) "Nguyen Minh Trung"
 2) "Vo Van Vu"
 3) "Tan Minh Sang"
 4) "Dao Minh Di"
 5) "Tran Minh Quyen"
 6) "Pham Thi Chung"
 7) "Cao Van Do"
 8) "Tran Trung Duc"
 9) "Le Minh Cao"
10) "Nguyen Dinh Bao"
11) "Nguyen Van An"
12) "Tran Minh Sang"
localhost:6379> LINSERT students BEFORE "Dao Minh Di" "Dao Minh Duc"
(integer) 13
localhost:6379> LRANGE students 0 -1
 1) "Nguyen Minh Trung"
 2) "Vo Van Vu"
 3) "Tan Minh Sang"
 4) "Dao Minh Duc"
 5) "Dao Minh Di"
 6) "Tran Minh Quyen"
 7) "Pham Thi Chung"
 8) "Cao Van Do"
 9) "Tran Trung Duc"
10) "Le Minh Cao"
11) "Nguyen Dinh Bao"
12) "Nguyen Van An"
13) "Tran Minh Sang"
localhost:6379> RPOP students
"Tran Minh Sang"
localhost:6379> LPOP students
"Nguyen Minh Trung"
localhost:6379> LRANGE students 0 -1
 1) "Vo Van Vu"
 2) "Tan Minh Sang"
 3) "Dao Minh Duc"
 4) "Dao Minh Di"
 5) "Tran Minh Quyen"
 6) "Pham Thi Chung"
 7) "Cao Van Do"
 8) "Tran Trung Duc"
 9) "Le Minh Cao"
10) "Nguyen Dinh Bao"
11) "Nguyen Van An"
```

**Common use cases for lists**

- Remember the latest updates posted by users into a social network.
- Communication between processes, using a consumer-producer pattern where the producer pushes items into a list,
and a consumer (usually a worker) consumes those items and executed actions.
Redis has special list commands to make this use case both more reliable and efficient.
- Lastest Twitter social network

-To describe a common use case step by step, imagine your home page shows the latest photos published
in a photo sharing social network and you want to speedup access.
Every time a user posts a new photo, we add its ID into a list with LPUSH.
When users visit the home page, we use LRANGE 0 9 in order to get the latest 10 posted items.

**3.Hashes**

- Ref : https://www.openmymind.net/Data-Modeling-In-Redis/
