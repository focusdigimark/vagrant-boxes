# Web Elements

## 1.Boxes

### ROR

1.Nginx
2.RVM : http://nhatkiduan.misostack.com/article/ruby-001-installation/
3.SSL

Application: ERP

### LAMP

1.Nginx
2.PHP FPM

Application: Ecommerce

### MEAN(MongoDB + ExpressJS + AngularJS + Nginx)

- NodeJS
- Application: Realtime

### Python

1.Nginx
2.Python
3.NodeJS

Application: Odoo

## 2.Window services

### Databases

- postgresql
- mysql
- mongodb

### Code IDE

- Rubymine
- Sublime
- VisualCode

## 3.Cloud services

- AWS
- Mailer
- Sentry
...

# Window Software

1.Facebook Marketing: fast + cheap
2.SEO: fast + cheap
3.Small Management Software ( allow backup on drive for daily ): private + backup + cheap

# Automation

1.Language: C & C++
2.Technology: Arduno 